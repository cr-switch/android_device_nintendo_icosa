# Inherit some common carbon stuff.
$(call inherit-product, vendor/carbon/config/common_tablet.mk)

# Inherit device configuration for icosa.
$(call inherit-product, device/nintendo/icosa/full_icosa.mk)

PRODUCT_NAME := carbon_icosa
PRODUCT_DEVICE := icosa
